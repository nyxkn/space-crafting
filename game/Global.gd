extends Node

###########################################
### this needs to be added to autoloads ###
###########################################

# define global variables in here
# useful for static variables for classes (can't have them inside the actual classes)
	
enum ItemType { NONE, ORE, COMPONENT }

var items := {
	aluminum = [ "Aluminum Ore", ItemType.ORE ],
	iron = [ "Iron Ore", ItemType.ORE ],
	copper = [ "Copper Ore", ItemType.ORE ],
	sand = [ "Sand Block", ItemType.ORE ],

	structure = [ "Aluminum Panel", ItemType.COMPONENT ],
	mechanic = [ "Mechanical Component", ItemType.COMPONENT ],
	electric = [ "Copper Conductor", ItemType.COMPONENT ],
	silicon = [ "Silicon Boule", ItemType.COMPONENT ],

	coin = [ "Coins", ItemType.NONE ],
}

var refine_map := {
	aluminum = 'structure',
	iron = 'mechanic',
	copper = 'electric',
	sand = 'silicon',
	}

#var Item = {}
#
#func _init() -> void:
#	id = 0
#	for i in items_list.keys():
#		Item[i] = id
#		id += 1

var upgrades_fields := [ "title", "cost" ]
var upgrades_data := {
	automine1 = [ "Automine", 2 ],
	fasterautomine1 = [ "Faster Automine 1", 5 ],
	fasterautomine2 = [ "Faster Automine 2", 10 ],
	fasterautomine3 = [ "Faster Automine 3", 25 ],
	fasterautomine4 = [ "Faster Automine 4", 50 ],
	fasterautomine5 = [ "Faster Automine 5", 100 ],
	fasterautomine6 = [ "Faster Automine 6", 200 ],
	fasterautomine7 = [ "Faster Automine 7", 400 ],
	fasterautomine8 = [ "Faster Automine 8", 800 ],
	fasterautomine9 = [ "Faster Automine 9", 1600 ],
	fasterminting1 = [ "Faster Minting 1", 5  ],
	fasterminting2 = [ "Faster Minting 2", 10 ],
	fasterminting3 = [ "Faster Minting 3", 25 ],
	fasterminting4 = [ "Faster Minting 4", 50 ],
	fasterminting5 = [ "Faster Minting 5", 100 ],
	fasterminting6 = [ "Faster Minting 6", 200  ],
	fasterminting7 = [ "Faster Minting 7", 400 ],
	fasterminting8 = [ "Faster Minting 8", 800 ],
	fasterminting9 = [ "Faster Minting 9", 1600 ],
	fasterrefining1 = [ "Faster Refining 1", 5 ],
	fasterrefining2 = [ "Faster Refining 2", 10],
	fasterrefining3 = [ "Faster Refining 3", 25],
	fasterrefining4 = [ "Faster Refining 4", 50],
	fasterrefining5 = [ "Faster Refining 5", 100 ],
	fasterrefining6 = [ "Faster Refining 6", 200],
	fasterrefining7 = [ "Faster Refining 7", 400],
	fasterrefining8 = [ "Faster Refining 8", 800],
	fasterrefining9 = [ "Faster Refining 9", 1600 ],
	miningconcurrent2 = [ "2x Concurrent Automine", 100 ],
	miningconcurrent3 = [ "3x Concurrent Automine", 200 ],
	miningconcurrent4 = [ "4x Concurrent Automine", 400 ],
	# refiningconcurrent2 = [ "2x Concurrent Refining", 100 ],
	# refiningconcurrent3 = [ "3x Concurrent Refining", 200 ],
	# refiningconcurrent4 = [ "4x Concurrent Refining", 400 ],

	}
var upgrades := {}

var ores := []
var components:= []

func _init() -> void:
	for id in items:
		if item_type(id) == ItemType.ORE:
			ores.push_back(id)
		elif item_type(id) == ItemType.COMPONENT:
			components.push_back(id)

	upgrades = array_data_to_named_dictionary(upgrades_data, upgrades_fields)


func array_data_to_named_dictionary(var data, var fields) -> Dictionary:
	var dict = {}
	for id in data:
		dict[id] = {}
		dict[id].id = id
		for i in fields.size():
			var field_name = fields[i]
			dict[id][field_name] = data[id][i]
	return dict


func item_name(id):
	return items[id][0]
	
func item_type(id):
	return items[id][1]
	

func item_id_from_name(item_name):
	return Utils.dict_get_key_of_value(items, item_name)	
	
func source_ore_id_from_component(component_id):
	return Utils.dict_get_key_of_value(refine_map, component_id)
