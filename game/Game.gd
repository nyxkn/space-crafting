extends Node

# signals

# enums

# constants



# exported variables

# public variables

# private variables

# onready variables

onready var upgrades = $UI/Upgrades
onready var mining = $UI/Mining
onready var refining = $UI/Refining
onready var minting = $UI/Minting


################
# godot functions
################

func _enter_tree() -> void:
	pass
#	Pause.pausing_allowed = true


func _ready() -> void:
	$Debug.add_reference(Inv.item_quantities, "item_quantities")
	
	upgrades.connect("upgrade_activated", self, "upgrade_activated")
#	$UI/Mine.connect("pressed", $UI/Mining, "_on_Mine_pressed")
	
	mining.mining_time = 5
	mining.mining_upgrade = 0.6
	
	minting.processing_time = 3
	minting.processing_upgrade = 0.5
	refining.processing_time = 6
	refining.processing_upgrade = 0.7
	
#	Inv.add_item("coin", 1000)


func _exit_tree() -> void:
	pass


func _input(event: InputEvent) -> void:
	pass
#	if event.is_action_pressed("ui_home"):
#		Framework.change_tree(Config.MAIN_MENU)


func _process(delta: float) -> void:
	pass

		
#	$UI/Mining/VBox/Ores.get_child(0).resize_area2d()


################
# functions
################


################
# signals
################

func upgrade_activated(id):
	if id == "automine1":
		mining.activate_automine()
	elif id.begins_with("fasterautomine"):
		mining.upgrade_automine_time()
	elif id.begins_with("fasterrefining"):
		refining.upgrade_processing_time()
	elif id.begins_with("fasterminting"):
		minting.upgrade_processing_time()
	elif id.begins_with("miningconcurrent"):
		mining.upgrade_concurrent()
	elif id.begins_with("refiningconcurrent"):
		refining.upgrade_concurrent()


