extends MarginContainer
class_name UpgradeEntry

signal upgrade_unlocked(id)

var id = ""
var title = ""
var cost = -1

func _ready() -> void:
	pass


func init(id: String):
	self.id = id
#	var upgrade = G.upgrades[id]
	title = G.upgrades[id].title
	cost = G.upgrades[id].cost
	$Upgrade.text = title
	$MarginContainer/Cost.text = str(cost)
	
	Inv.connect("quantity_changed", self, "quantity_changed")
	
	return self


func _on_Upgrade_pressed() -> void:
	if Inv.get_quantity("coin") >= cost:
		Inv.add_item("coin", -cost)
		emit_signal("upgrade_unlocked", id)
		hide()
		queue_free()
	else:
		print("too broke")


func quantity_changed(item_id, new_quantity):
	if item_id == "coin":
		$Upgrade.disabled = new_quantity < cost
