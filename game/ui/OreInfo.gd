extends Control


func _ready() -> void:
	pass


func setup(ore_id):
	find_node("Label").text = G.item_name(ore_id)
	find_node("TrackingCount").setup(ore_id)
