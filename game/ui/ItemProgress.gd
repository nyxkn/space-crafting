extends HBoxContainer

var item_id := -1

func _ready() -> void:
	$MarginContainer/Border.border_color = Color.royalblue


func setup(item_id) -> void:
	self.item_id = item_id
	
	$MarginContainer/VBoxContainer/Item.text = G.item_name(item_id)
	$TrackingCount.setup(item_id)
