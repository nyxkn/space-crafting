extends Control


#signal docked
#signal selection_toggled(ore_name)

var dragging = false
#var returning = false
#var docked_to = Vector2.ZERO
var ore_name = ""
var ore_id := ""

func _ready() -> void:
	pass
#	$MiningEntry/MarginContainer/Border.border_color = Color.royalblue
	
func resize_area2d():
	var rect = $MiningEntry
	var hx = rect.rect_size.x / 2
	print(hx)
	var hy = rect.rect_size.y / 2
	$Draggable.position = Vector2(hx, hy)
	$Draggable/CollisionShape2D.shape.extents = Vector2(hx, hy)


func setup(ore_id):
	self.ore_id = ore_id
	ore_name = G.item_name(ore_id)
	add_to_group("ores")
	
	$MiningEntry/MarginContainer/ItemProgress.setup(ore_id)
	
#	var ore_btn: Button = find_node("Selection")
#	ore_btn.text = ore_name
	
#	find_node("TrackingCount").tracking_item_id = ore_id

func _process(delta: float) -> void:
	if dragging:
#		$DragClone.rect_global_position = lerp(
#			$DragClone.rect_global_position, get_global_mouse_position() - $Draggable.position, delta * 25)

		drag_clone.rect_global_position = lerp(
			drag_clone.rect_global_position, get_global_mouse_position() - $Draggable.position, delta * 25)


#	elif returning:
#		# replace this with a tween so you can hide when finished
#		$DragClone.rect_global_position = lerp($DragClone.rect_global_position, docked_to, delta * 10)


func _input(event: InputEvent) -> void:
	if dragging:
		if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.pressed:
			dragging = false
#			returning = true
			drag_clone.hide()
			drag_clone.queue_free()
			print("dropp")

			for a in get_tree().get_nodes_in_group("drop_area"):
				if a.selected:
#					docked_to = a.rect_global_position
#					a.current_item = 
					a.set_docked(ore_id)

			get_tree().call_group("drop_area", "set_active", false)


var drag_clone: Control

func _on_Draggable_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event.is_action_pressed("click") and dragging == false:
		print("begin drag")
		dragging = true
		get_tree().call_group("drop_area", "set_active", true)
#		$DragClone.show()
		drag_clone = $MiningEntry/MarginContainer/ColorRect.duplicate()
		drag_clone.rect_global_position = $MiningEntry/MarginContainer/ColorRect.rect_global_position
		$CanvasLayer.add_child(drag_clone)
#		docked_to = rect_global_position



func _on_MiningEntry_minimum_size_changed() -> void:
	resize_area2d()
