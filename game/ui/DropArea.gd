extends Control

signal item_docked(item_id)

var active = false setget set_active
var selected = false
var current_item := ""

onready var colorRect = $Center/ColorRect

var color_off = Color(100)
#var color_drag = Color.lightblue
var color_drag = Color.brown
var color_select = Color.royalblue

func _ready() -> void:
	colorRect.color = Color(150)


func set_docked(item_id):
	current_item = item_id
	emit_signal("item_docked", item_id)


func set_active(value):
	active = value
	if active:
		colorRect.color = color_drag
	else:
		deselect()
		colorRect.color = color_off


func select():
	selected = true
	colorRect.color = color_select


func deselect():
	selected = false
	colorRect.color = color_drag


func _on_Area2D_mouse_entered() -> void:
	if active: select()


func _on_Area2D_mouse_exited() -> void:
	if active: deselect()
