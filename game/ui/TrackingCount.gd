extends Control


var tracking_item_id

func _ready() -> void:
	Inv.connect("quantity_changed", self, "quantity_changed")
	

func setup(item_id):
	tracking_item_id = item_id
	$TrackingCount.text = str(Inv.get_quantity(tracking_item_id))
	
	
func quantity_changed(item_id, new_quantity):
	if tracking_item_id == item_id:
		$TrackingCount.text = str(new_quantity)
