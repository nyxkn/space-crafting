extends Node

func _ready():
	# entry point for the whole application

	# custom config settings
	Config.HIDE_LOG_LEVEL = {
		Log.LogLevel.DEBUG: false,
		Log.LogLevel.INFO: false,
	}
	
#	FPS.enabled = true

	# load other settings from file
#	Config.loadConfig()
#	Log.i("MAIN", "Config loaded")

#	Framework.change_scene(Config.INTRO_SCREEN, false)
	Framework.change_scene(Config.MAIN_MENU, false)
#	Framework.change_scene(Config.NEW_GAME, false)
#	Framework.change_scene("res://screens/Intro.tscn", false)

#func _input(event):
#	print(event)
