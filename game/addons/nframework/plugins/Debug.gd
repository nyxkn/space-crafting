extends Node

# to use: add the debug scene to your scene
# then add things to track with add_reference and add_funcref

# if you move the $Lines node to a different corner, adjust Grow Direction


class Ref:
	var ref
	var tag
	
	func _init(ref, tag):
		self.ref = ref
		self.tag = tag

var refs := []
var funcrefs := []

onready var ui_lines = $Lines


func _ready():
	pass


func _process(delta):
	ui_lines.text = ''

	for r in refs:
#		_add_line(str(r))
		_add_data(r.ref, r.tag)

	for r in funcrefs:
		var data = r.ref.call_func()
		_add_data(data, r.tag)


func _add_data(data, tag):
	if typeof(data) == TYPE_ARRAY:
		_add_text_line(tag)
		for i in data.size():
			_add_line(str(data[i]), str("[", i, "]"))
	elif typeof(data) == TYPE_DICTIONARY:
		_add_text_line(tag)
		for k in data:
			_add_line(str(data[k]), str(k))
	else:
		# assume basic unreferenceable type
#		_add_line(data if typeof(data) == TYPE_STRING else str(data))
		_add_line(str(data), tag)


func _add_line(value, tag = ""):
	var prefix = ""
	if tag != "": prefix = str(tag, ": ")
	_add_text_line(str(prefix, value))


func _add_text_line(text):
	ui_lines.text += text + "\n"


################################
# public functions

func add_reference(ref, tag = ""):
	refs.append(Ref.new(ref, tag))

# if it's a base type (int, float, string, vectors) and can't be passed as reference
# make a function that returns the data and pass that instead
# if data is passed as reference you can add it with add_reference

func add_funcref(f, tag = ""):
#	funcrefs.append(f)
	funcrefs.append(Ref.new(f, tag))
