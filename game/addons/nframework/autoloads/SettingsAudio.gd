extends Node

var volume_master: float = 0.0 setget set_volume_master
var volume_music: float = 0.0 setget set_volume_music
var volume_sfx: float = 0.0 setget set_volume_sfx

var volumes = {}
var buses = {}

var volume_master_bus: int
var volume_music_bus: int
var volume_sfx_bus: int

enum BUS { Master, Music, SFX }


#func init_volumes() -> void:
#	buses[BUS.Master] = AudioServer.get_bus_index("Master")
#	buses[BUS.Music] = AudioServer.get_bus_index("Music")
#	buses[BUS.SFX] = AudioServer.get_bus_index("SFX")
#
#	for b in BUS:
#		volumes[b] = db2linear(AudioServer.get_bus_volume_db(buses[b]))
#		set_volume(b)


func init_volumes() -> void:
	volume_master_bus = AudioServer.get_bus_index("Master")
	volume_music_bus = AudioServer.get_bus_index("Music")
	volume_sfx_bus = AudioServer.get_bus_index("SFX")

	var master := db2linear(AudioServer.get_bus_volume_db(volume_master_bus))
	var music := db2linear(AudioServer.get_bus_volume_db(volume_music_bus))
	var sfx := db2linear(AudioServer.get_bus_volume_db(volume_sfx_bus))

	set_volume_master(master)
	set_volume_music(music)
	set_volume_sfx(sfx)


func set_volume(bus: int, volume: float) -> void:
	match bus:
		BUS.Master: set_volume_master(volume)
		BUS.Music: set_volume_music(volume)
		BUS.SFX: set_volume_sfx(volume)


func get_volume(bus: int) -> float:
	var volume: float
	match bus:
		BUS.Master: volume = volume_master
		BUS.Music: volume = volume_music
		BUS.SFX: volume = volume_sfx
	return volume


#func get_bus_id(bus: int) -> int:
#	var bus_id: int
#	match bus:
#		BUS.Master: bus = volume_master_bus
#		BUS.Music: bus = volume_music_bus
#		BUS.SFX: bus = volume_sfx_bus
#	return bus_id


func set_volume_master(volume: float) -> void:
	volume_master = volume
	AudioServer.set_bus_volume_db(volume_master_bus, linear2db(volume))


func set_volume_music(volume: float) -> void:
	volume_music = volume
	AudioServer.set_bus_volume_db(volume_music_bus, linear2db(volume))


func set_volume_sfx(volume: float) -> void:
	volume_sfx = volume
	AudioServer.set_bus_volume_db(volume_sfx_bus, linear2db(volume))
