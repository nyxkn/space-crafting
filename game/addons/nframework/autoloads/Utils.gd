class_name Utils

## Generic utility functions

static func array_to_printable_string(array) -> String:
	var string := ""
	for e in array:
		string += str(e, ", ")
	return string.substr(0, string.length() - 2)

# this would ideally be a variadic function
static func debug_print(varargs_array) -> void:
	print(array_to_printable_string(varargs_array))


# random simple benchmark function. call this for 10-20 times to simulate a couple seconds lock
static func benchmark_function() -> void:
	var lst = []
	for i in 999999:
		lst.append(sqrt(i))


static func set_margins(margin_container, margin_value: int, margin_rightleft: int = -1) -> void:
	var mc: MarginContainer = margin_container

	if margin_rightleft >= 0:
		mc.add_constant_override("margin_right", margin_rightleft)
		mc.add_constant_override("margin_left", margin_rightleft)
	else:
		mc.add_constant_override("margin_right", margin_value)
		mc.add_constant_override("margin_left", margin_value)		
		
	mc.add_constant_override("margin_top", margin_value)
	mc.add_constant_override("margin_bottom", margin_value)


# node: node to recurse into. children: array to store children in
static func store_children_recursive(node: Node, children: Array = [], recurse_level: int = 0) -> void:
#	Log.d("UTILS", "    ".repeat(recurse_level) + "[" + node.name + "]")
	
	for n in node.get_children():
		if n.get_child_count() > 0:
			children.append(n)
			store_children_recursive(n, children, recurse_level + 1)
		else:
			children.append(n)
#			Log.d("UTILS", "    ".repeat(recurse_level + 1) + "- " + n.name)


static func grab_focus_on_mouse_entered(control: Control) -> void:
	control.connect("mouse_entered", control, "grab_focus")


static func setup_focus_grabs_on_mouse_entered(control: Control) -> void:
	var children: Array = []
	store_children_recursive(control, children)

	for c in children:
		if c is Button or c is Slider:
			grab_focus_on_mouse_entered(c)


# look into a dictionary to find the desired value. return its key.
static func dict_get_key_of_value(dict: Dictionary, value_search):
	for k in dict.keys():
		if k == value_search:
			return k
	return null
