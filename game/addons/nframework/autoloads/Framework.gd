extends Node

## Framework is pretty much just a collection of useful game functions or helpers

# export wouldn't work from here. it should be a scene
#export (String, FILE, "*.tscn") var MAIN_MENU: String = "res://addons/nframework/screens/MainMenu.tscn"
# or this should point to defaults, but a config file load could override this
# or poor man's version we just override this manually in game code

# you can possibly add changing scene through signals, that is:
#   emit_signal("change_scene", MAIN_MENU)
# this makes more sense if you're trying to decouple things,
# but since this is an autoload it's pointless really. just call directly

# CANVAS LAYERS
# 0 is by default the default scene layer for non-canvaslayer things
# 1 is for canvaslayers for in-game gui
# 2 is for canvaslayers overlays that fit on top of the gui
# 9 is for canvaslayers debug overlays - always on top


signal scene_faded_out
signal scene_changed
signal scene_faded_in


onready var root_viewport: Viewport = get_node("/root")


func _ready() -> void:
	pass


func _process(delta: float) -> void:
	pass


func change_scene(scene, with_transition: bool = true, transition_duration: float = 0.0) -> bool:
	if !ResourceLoader.exists(scene):
		Log.e("FRAMEWORK", "Attempting to load inexistent scene: " + scene)
		return false

	root_viewport.gui_disable_input = true
	
	# debatable whether this should be here or not
	# but it looks like the cleaner way of making sure we don't lose track of it
	Pause.pausing_allowed = false

	if with_transition:
		Transition.fade_out(transition_duration)
		yield(Transition, "fade_out_completed")
		emit_signal("scene_faded_out")
		
	get_tree().change_scene(scene)
	emit_signal("scene_changed")

	if with_transition:
		Transition.fade_in(transition_duration)
		yield(Transition, "fade_in_completed")
		emit_signal("scene_faded_in")

	root_viewport.gui_disable_input = false
	
	# we probably don't need to signal this. rather wait for function return?
#	emit_signal("scene_changed")
#	print("scene_changed")

	return true




### Frame waiting mechanic. Add to separate file if you need _process for other things
# useless! you can just use this instead: yield(get_tree(), "idle_frame")

#var frames_to_wait: int = 0
#signal frames_elapsed
#
#func _ready() -> void:
#	set_process(false)
#
#func _process(delta) -> void:
#	print ("processing")
#	if frames_to_wait > 0:
#		frames_to_wait -= 1
#	else:
#		emit_signal("frames_elapsed")
#		set_process(false)
#
#func wait_frames(var frames: int) -> void:
#	if frames > 0:
#		frames_to_wait = frames
#		set_process(true)

