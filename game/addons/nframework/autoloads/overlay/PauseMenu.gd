extends CanvasLayer

# public
var paused: bool = false setget toggle_paused
# each scene is responsible for enabling pausing_allowed.
# it is automatically disabled in Framework.change_scene()
var pausing_allowed: bool = false setget set_pausing_allowed

# private
var settings_opened := false
var settings_menu: Control = null

onready var btn_Resume: Button = find_node("Resume")
onready var SettingsMenu = load(Config.SETTINGS_MENU)


func _ready():
	$Control.theme = load(Config.THEME)
	$Control.visible = false
	Utils.setup_focus_grabs_on_mouse_entered($Control)
		
		
func _input(event) -> void:
	if event.is_action_pressed("ui_cancel"):
		if paused:
			if !settings_opened:
				self.paused = false
		else:
			self.paused = true
			print("pausing")


func set_pausing_allowed(value: bool) -> void:
	pausing_allowed = value
	set_process_input(pausing_allowed)


func toggle_paused(value: bool) -> void:
	print("toggle_paused")
	paused = value
	print(str("pause ", paused))
	get_tree().paused = paused
	$Control.visible = paused
	if paused:
		btn_Resume.grab_focus()


func _on_Resume_pressed():
	self.paused = false


func _on_MainMenu_pressed() -> void:
	Framework.change_scene(Config.MAIN_MENU)
	
	# TransitionOverlay has to be set to process during pause for this to work
	# otherwise just do the change_scene without transitions
	yield(Framework, "scene_faded_out")
	self.paused = false

	
func _on_Settings_pressed() -> void:
	settings_opened = true
	settings_menu = SettingsMenu.instance()
	settings_menu.connect("menu_closed", self, "on_SettingsMenu_closed")
	$Control.add_child(settings_menu)
	
	
func on_SettingsMenu_closed() -> void:
#	remove_child(settings_menu)
	settings_menu.queue_free()
	btn_Resume.grab_focus()
#	settings_opened = false
