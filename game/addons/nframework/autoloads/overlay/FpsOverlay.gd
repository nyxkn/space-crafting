extends CanvasLayer


var enabled: bool setget set_enabled

var update_interval: float = 1
var timer: float


func _ready() -> void:
	$Control.theme = load(Config.THEME)
	set_enabled(false)
	$Control/FPS.text = ""


func _process(delta: float) -> void:
	timer += delta
	if timer > update_interval:
		timer = 0
		$Control/FPS.text = str(Engine.get_frames_per_second())
	

func set_enabled(value: bool) -> void:
	enabled = value
	set_process(enabled)
	$Control.visible = enabled
