extends Control

var keypress_enabled := true

func _ready() -> void:
	yield(get_tree().create_timer(3), "timeout")
#	Framework.change_scene(Config.NEW_GAME)
	Framework.change_scene(Config.MAIN_MENU)

func _input(event) -> void:
	if keypress_enabled:
		if event.is_action_pressed("ui_accept") or event.is_action_pressed("ui_select") or event.is_action_pressed("ui_cancel"):
			Framework.change_scene(Config.MAIN_MENU)
