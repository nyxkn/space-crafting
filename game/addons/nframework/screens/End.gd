extends Control

var keypress_enabled := false

func _ready() -> void:
	yield(get_tree().create_timer(1), "timeout")
	keypress_enabled = true

func _input(event) -> void:
	if keypress_enabled:
		if event.is_action_pressed("ui_accept") or event.is_action_pressed("ui_select") or event.is_action_pressed("ui_cancel"):
			Framework.change_scene(Config.MAIN_MENU)
