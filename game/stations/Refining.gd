extends Processing

func _ready() -> void:
	pass


func processing_init(ore_id):
	.processing_init(ore_id)
	
	var item_progress = ItemProgress.instance()
	item_progress.setup(G.refine_map[ore_id])
	$Items.add_child(item_progress)


func processing_cancel():
	.processing_cancel()

	for i in $Items.get_children():
		i.queue_free()


func processing_cycle_completed():
	Inv.add_item(G.refine_map[processing_ore_current], processing_block_size)
	.processing_cycle_completed()


