extends Node
class_name Processing

const ItemProgress := preload("res://ui/ItemProgress.tscn")

var processing_paused = true

var processing_ore_current := ""
var processing_block_size := 1
var processing_time = 3
var processing_upgrade = 0.8
var processing_concurrent_max = 1


onready var processing_timer = $Timer

func _ready() -> void:
	Inv.connect("quantity_changed", self, "quantity_changed")
	$OreInfo.hide()
	$Cancel.hide()

func _process(delta: float) -> void:
	if processing_ore_current != "":
		var progress_bar = $Items.get_child(0).find_node("ProgressBar")
		if $Timer.is_stopped():
			progress_bar.value = 0
		else:
			progress_bar.value = inverse_lerp($Timer.wait_time, 0, $Timer.time_left)


func quantity_changed(item_id, new_quantity):
	if processing_paused:
		if item_id == processing_ore_current:
			processing_start()


func processing_init(ore_id):
	print("processing init")
	processing_ore_current = ore_id
	$OreInfo.setup(processing_ore_current)
	$OreInfo.show()
	$Cancel.show()
	
	# HACK
	get_tree().root.find_node("Mining", true, false).get_ore_node(ore_id).find_node("Border", true, false).show()
	

func processing_start():
	print("processing start")
	if processing_allowed(processing_ore_current):
		processing_timer.wait_time = processing_time
		processing_timer.start()
		processing_paused = false	

	
func processing_allowed(ore_id) -> bool:
	if Inv.get_quantity(ore_id) >= processing_block_size:
		return true
	return false


func processing_cycle_completed():
	Inv.add_item(processing_ore_current, -1)

	if not processing_allowed(processing_ore_current):
		processing_pause()


func processing_pause():
	print("processing stop")
	processing_paused = true
	processing_timer.stop()
	

func processing_cancel():
	print("processing cancelled")
	
	processing_pause()
	
	# HACK
	if processing_ore_current != "":
		get_tree().root.find_node("Mining", true, false).get_ore_node(processing_ore_current).find_node("Border").hide()

	processing_ore_current = ""
	$OreInfo.hide()
	$Cancel.hide()



func _on_DropArea_item_docked(item_id) -> void:
	if processing_ore_current == item_id:
		return
	
	var ref = get_tree().root.find_node("Refining", true, false)
	var mint = get_tree().root.find_node("Minting", true, false)
	
	# HACK
	if self.name == "Minting":
		if ref.processing_ore_current == item_id:
			ref.processing_cancel()
	
	if self.name == "Refining":
		if mint.processing_ore_current == item_id:
			mint.processing_cancel()

	processing_cancel()
	processing_init(item_id)
	processing_start()


func _on_Timer_timeout() -> void:
	processing_cycle_completed()


func _on_Cancel_pressed() -> void:
	processing_cancel()


func upgrade_processing_time():
	processing_time *= processing_upgrade
	$Timer.wait_time = processing_time
	print("processing time: ", str(processing_time))

func upgrade_concurrent():
	processing_concurrent_max += 1
