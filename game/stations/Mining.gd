extends Panel


const MiningEntry := preload("res://ui/MiningEntry.tscn")

var mining_ores_current = {}
var mining_concurrent_max = 1
var automining = false
var mining_time = 5
var mining_upgrade = 0.6

var ore_toggle_history = []
var ore_position = {}
var ore_buttons

func _ready() -> void:
	ore_toggle_history.resize(3)
	
	var position = 0
	
	for id in G.ores:
		mining_ores_current[id] = false

		var entry = MiningEntry.instance()
		entry.setup(id)
		$VBox/Ores.add_child(entry)
		entry.find_node("Selection").connect("toggled", self, "ore_toggled", [id])

		ore_position[id] = position
		position += 1

	ore_buttons = get_tree().get_nodes_in_group("ore_toggle")
	ore_buttons[0].pressed = true
#	ore_toggled(true, "aluminum")

func _process(delta: float) -> void:
	if automining:
		for o in mining_ores_current:
			if mining_ores_current[o]:
				var progress_bar = $VBox/Ores.get_child(ore_position[o]).find_node("ProgressBar")
				progress_bar.value = inverse_lerp($Automine.wait_time, 0, $Automine.time_left)


func ore_toggled(pressed, ore_id):
	mining_ores_current[ore_id] = pressed


	var toggled_count = 0
	for o in mining_ores_current.values():
		toggled_count += int(o)
		
	print(toggled_count)
	
	# re-enable ore if trying to deactivate the only one
	if toggled_count == 0:
		mining_ores_current[ore_id] = true
		ore_buttons[ore_position[ore_id]].pressed = true
		return
	
	if pressed:
		if toggled_count > mining_concurrent_max:
			var ore_to_disable = ore_toggle_history[mining_concurrent_max - 1]
			ore_buttons[ore_position[ore_to_disable]].pressed = false
			mining_ores_current[ore_to_disable] = false

		ore_toggle_history.push_front(ore_id)
		ore_toggle_history.pop_back()
	
		print(ore_toggle_history)


func get_ore_node(ore_id):
	return $VBox/Ores.get_child(ore_position[ore_id])

func mine_current(quantity):
	for ore_id in mining_ores_current:
		if mining_ores_current[ore_id] == true:
			Inv.add_item(ore_id, quantity)


func _on_Mine_pressed() -> void:
	mine_current(1)


func activate_automine():
	$Automine.wait_time = mining_time
	$Automine.start()
	automining = true
	

func _on_Automine_timeout() -> void:
	mine_current(1)

func upgrade_automine_time():
	mining_time *= mining_upgrade
	$Automine.wait_time = mining_time
	print("automine time: ", str(mining_time))

func upgrade_concurrent():
	mining_concurrent_max += 1
	
