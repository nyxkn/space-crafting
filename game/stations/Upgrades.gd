extends Panel

signal upgrade_activated(upgrade_id)

const UpgradeEntry := preload("res://ui/UpgradeEntrry.tscn")

class Sorter:
	static func sort_ascending(a, b):
		if a.cost < b.cost:
			return true
		return false

var by_cost = []

var active = []

func _ready() -> void:
	var array = []
#	for id in G.upgrades:
#		var e = G.upgrades[id]
#		e.id = id
#		array.push_back(e)
	for u in G.upgrades.values():
		array.push_back(u)
		
	by_cost = array
	by_cost.sort_custom(Sorter, "sort_ascending")
	
	for u in by_cost:
		var upgrade_entry: UpgradeEntry = UpgradeEntry.instance().init(u.id)
		$Content/ScrollContainer/UpgradesList.add_child(upgrade_entry)
		upgrade_entry.connect("upgrade_unlocked", self, "upgrade_unlocked")
		
	$Content/HBoxContainer/TrackingCount.setup("coin")

func upgrade_unlocked(upgrade_id):
	active.push_back(upgrade_id)
	emit_signal("upgrade_activated", upgrade_id)

