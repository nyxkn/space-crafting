extends Processing


var coin_item_id = "coin"

func _ready() -> void:
	var item_progress = ItemProgress.instance()
	item_progress.setup(coin_item_id)
	$Items.add_child(item_progress)
	item_progress.hide()


func processing_init(ore_id):
	.processing_init(ore_id)
	
	$Items.get_child(0).show()
	
#	var item_progress = ItemProgress.instance()
#	item_progress.setup(G.refine_map[ore_id], ore_id)
#	$UI/Refining/Items.add_child(item_progress)


func processing_cancel():
	.processing_cancel()

	$Items.get_child(0).hide()

func processing_cycle_completed():
	Inv.add_item(coin_item_id, processing_block_size)
	.processing_cycle_completed()
