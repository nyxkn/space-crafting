extends Panel

const ItemCount := preload("res://ui/ItemCount.tscn")

func _ready() -> void:
	Inv.connect("quantity_changed", self, "quantity_changed")

#func update_item():
#	pass

#func add_item():
#	var item = ItemCount.instance()
##	item.find_node("Item").text = 

func quantity_changed(item_id, new_quantity):
#	if G.components.has(item_id):
	var holdList = $VBoxContainer/HoldList
	if G.item_type(item_id) == G.ItemType.COMPONENT:
		var item_entry = holdList.get_node_or_null(str(item_id))
		if item_entry == null:			
			var item = ItemCount.instance()
			item.name = str(item_id)
			item.get_node("Item").text = G.item_name(item_id)
			item.get_node("TrackingCount").setup(item_id)
			holdList.add_child(item)
