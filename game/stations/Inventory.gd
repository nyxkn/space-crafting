extends Node

signal quantity_changed(item_id, new_quantity)

const ItemCount := preload("res://ui/ItemCount.tscn")


var item_quantities = {}

func _init():
	for k in G.items: item_quantities[k] = 0


func add_item(item_id, quantity = 1):
	item_quantities[item_id] += quantity
	emit_signal("quantity_changed", item_id, item_quantities[item_id])
	
#	if item_id == "coin":
#		emit_signal("coin_changed", item_quantities.coin)


func get_quantity(item_id):
	return item_quantities[item_id]	
